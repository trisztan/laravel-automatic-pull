# Laravel Automatic Pull

Laravel Automatic Pull is a cron based shell script. You can easily fetch master branch every x minutes.

## How to setup?

- You need to pull laravel project on server.
- Configure enviroment file.
- Create or download {shellscript}.sh file.
- Set laravel project folder in shell script.
- Type crontab -e and add the following line and set path to your shell script file:
~~~~
*/10 * * * * sh /var/www/example.com/{shellscript}.sh >/dev/null 2>&1
~~~~

- Enjoy it.
