#!/bin/sh
cd /var/www/example.com/
git fetch --all
git reset --hard origin/master
php artisan migrate:reset
php artisan migrate --seed
